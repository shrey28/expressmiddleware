const express = require("express");
const app = express();

const morgan = require("morgan")

//use morgan middleware
app.use(morgan("dev"))


//create middleware is function a req and res object

// const logger = (req, res, next) => {
// //any logic here
// console.log('Am a logger middleware')
// next()

// };

// use middleware
// app.use(function(req,res,next){
//     console.log('Am a logger middleware')
//     next()
// })

//chaining middleware

// app
//   .use((req, res, next) => {
//     console.log("First middleware");
//     next();
//   })
//   .use((req, res, next) => {
//     console.log("Second middleware");
//     next();
//   })
//   .use((req, res, next) => {
//     console.log("Third middleware");
//     next();
//   });

//proctected route middleware
// const protected = (req, res, next) => {
//   let userLoginDetails = {
//     isLogin: true,
//     username: "Shrey",
//   };
//   if (userLoginDetails.isLogin) {
//     next();
//   } else {
//     return res.json("You must login first");
//   }
// };
// app.use(protected);

// const isAdmin = (req, res, next) => {
//   let userLoginDetails = {
//     isLogin: true,
//     username: "Shrey",
//     isAdmin:true
//   };
//   if (userLoginDetails.isAdmin) {
//     next();
//   } else {
//     return res.json("You not login first");
//   }
// };
// app.use(protected);

//coding challenge 1
// const logger = (req,res,next)=>{
//     console.log(`${req.method} ${req.url} ${new Data().toISOString()}`)
// //    console.log(new Date().toISOString())
// next()
// }
// app.use(logger)

//coding challenge 2

let requestCountVal = 0
const requestCount =(req,res,next)=>{
    requestCountVal++
    console.log(requestCountVal)
    if(requestCountVal>=10){
        return res.status(429).json('Too many request')
    }
    next()
}
app.use(requestCount)

app.get("/", (req, res) => {
  res.send("Server Is runnig Browser");
});

// app.get("/posts", (req, res) => {
//   res.json({ msg: "Posts Fetched" });
// });

// app.post("/create-post", (req, res) => {
//   res.json({ msg: "Posts Created" });
// });

// app.delete("/posts/:id", protected, isAdmin, (req, res) => {
//   res.json({ msg: "Post deleted" });
// });

app.listen(2000, () => {
  console.log("Connected 20000 Part");
});
